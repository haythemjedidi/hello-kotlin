fun sayHello_p(itemToGreet: String) = println("Hello $itemToGreet"); // top level function
fun sayHello_p2(greeting:String, itemToGreet: String) = println("$greeting $itemToGreet");

fun getGreeting(): String {
    return "Hello Kotlin"
}

fun sayHello():Unit { // fun sayHello()
    println(getGreeting());
}

fun getGreeting_2(): String? {
    return null
}

fun getGreeting_3() = "helloooooo" // Single expression function

fun sayHello_list(greeting: String, itemsToGreet: List<String>):Unit {
    itemsToGreet.forEach { itemToGreet ->
        println("$greeting $itemToGreet")
    }
    println(getGreeting());
}

fun getAverage(vararg input: Int): Float {
    var sum = 0.0f
    for (item in input) {
        sum += item
    }
    return (sum / input.size)
}

fun sayHello3(greeting: String, vararg itemsToGreet: String){
    itemsToGreet.forEach { itemToGreet ->
        println("$greeting $itemToGreet")
    }
}

fun greetPerson(greeting: String ="Hello", name:String = "BatMan") = println("$greeting $name")

fun main() {
    /*
    val name:String = "Kotlin";
    var greeting:String? = null;
    if(greeting != null){
        println(greeting);
    }else{
        println("hi")
    }
    println(name);



    when (greeting){
        null -> println("Hi")
        else -> println(greeting)
    }

    println(name)

    greeting = null;
    val greetingbeta = if(greeting != null) greeting else "Hii"
    println(greetingbeta);
    println(name)

    greeting = "Hello";
    val greetingbeta_2 = when (greeting) {
        null -> "Hiii"
        else -> greeting
    }
    println(greetingbeta_2);
    println(name)

    println("*********************************")
    println(getGreeting())
    println("*********************************")
    println(getGreeting_2())
    println("*********************************")
    sayHello()

    sayHello_p("Haythem");
    sayHello_p2("hello", "Bruce Wayne")
    println("*********************************")
    // */
    val interestingThings = arrayOf("Kotlin", "Programming", "Comic")
    println(interestingThings.size)
    println(interestingThings[0])
    println(interestingThings.get(index = 1))
    for(item in interestingThings) {
        print("$item ")
    }
    interestingThings.forEach { item ->
        println(item)
    }
    println("---------------------------------------------")
    interestingThings.forEachIndexed { index, s ->
        println("$index : $s")
    }
    println("---------------------------------------------")
    val interestingThings_l = listOf("Kotlin", "Programming", "Comic")
    println(interestingThings_l.size)
    val map= mapOf(1 to "a", 2 to "b", 3 to "c")
    map.forEach { (key, value) -> println("$key -> $value") }
    sayHello_list("Hi", interestingThings_l)

    val array = intArrayOf(1, 2, 3, 4)
    val result = getAverage(1, 2, 3, *array)
    println("res= $result")
    sayHello3("Hi", "java", "docker", "ansible" )

    greetPerson(greeting = "hiiiii", name = "Bruce Wayne")
    greetPerson(greeting = "hiiiii")
    greetPerson()
    val interestingThings_2 = arrayOf("java", "docker", "gitlab")
    sayHello3(greeting = "Hi",itemsToGreet = *interestingThings_2)

    println("---------------------------------------------")
    println("---------------------------------------------")
    println("---------------------------------------------")
    val person = Person()
    person.firstName
    person.lastName
    //person.nickName = "Bat Man"
    //person.nickName = "Bat-Man"
    //println(person.nickName)
    person.printInfo()
    //val second_Person = Person()

}
