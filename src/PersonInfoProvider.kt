interface PersonInfoProvider{
    val providerInfo : String
    fun printInfo(person: Person){
        println(providerInfo)
        person.printInfo()
        println("Parent")
    }
}

interface SessionInfoProvider{
    fun getSessionId() : String
}


open class BasicInfoProvider : PersonInfoProvider, SessionInfoProvider {
    override val providerInfo: String
        get() = "Basic Info Provider"

    protected open val sessionIdPrefix = "Session!!!"

    override fun printInfo(person: Person) {
        super.printInfo(person)
        println("Child")
    }

    override fun getSessionId(): String {
        return sessionIdPrefix
    }

    open fun isBasicInfoProvider(): String{
        return "Yes it is a BasicInfoProvider class!!!!"
    }
}


fun main(){


    //val provider = FancyInfoProvider()
    val provider = object : PersonInfoProvider{
        override val providerInfo: String
            get() = "New Info Provider"

        fun getSessionId() = "id"
    }
    val person = Person()
    provider.printInfo(person)
    println(provider.getSessionId())
    checkType(provider)
}


fun checkType(infoProvider: PersonInfoProvider){
    if (infoProvider !is FancyInfoProvider){
        println("is not a FancyInfoProvider")
    }else{
        println("is a FancyInfoProvider")
        val s = (infoProvider as FancyInfoProvider).providerInfo
        println(s)
        val isBasicInfoProvider = infoProvider.isBasicInfoProvider()
        println(isBasicInfoProvider)
    }
}