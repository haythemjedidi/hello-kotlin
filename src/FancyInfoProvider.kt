class FancyInfoProvider : BasicInfoProvider(){
    override val providerInfo: String
        get() = "FancyInfoProvider "
    override val sessionIdPrefix: String
        get() = "Fancy Session"
    override fun printInfo(person: Person) {
        println("Grand child")
    }

    override fun isBasicInfoProvider(): String {
        return "It is not basic, it's Fancy"
    }
}