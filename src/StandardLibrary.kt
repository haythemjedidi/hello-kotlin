fun main() {
    val list = listOf("Kotlin", "Java", "C++", "Javascript", null, null)
    list.filterNotNull()
        .filter {
            it.startsWith("J") or it.startsWith("K")
        }
        .map {
            it.length
        }
        .forEach {
        println(it)
    }
    println("----------------------------------------------")
    println("----------------------------------------------")
    println("----------------------------------------------")
    println("----------------------------------------------")

    list.filterNotNull()
        .associate {
            it.length to it
        }
        .forEach {
            println("${it.value}, ${it.key}")
    }
    println("----------------------------------------------")
    println("----------------------------------------------")
    println("----------------------------------------------")

    val first = list.first()
    println(first);
}