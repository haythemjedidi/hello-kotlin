class Person(val firstName:String = "Bruce", val lastName:String = "Wayne"){
    var nickName: String? = "BatMan"
        set(value) {
            field = value
            println("the new Nickname is $value")
        }
        get() {
            println("the returned nickname is $field")
            return field
        }

    constructor(nickName:String):this(){
        //nickName = _nickName
        println("Secondary constructor")
    }
    constructor(firstName:String = "Bruce", lastName:String, nickName:String):this(){
        //nickName = _nickName
        println("third constructor")
    }

    /*
    init {
        println("init 1")
    }

    init {
        println("init 2")
    }
    // */
    /*
    override fun toString(): String{
         return "$firstName $lastName $nickName"
    }
    // */

    fun printInfo(){
        val nickNameToPrint = nickName ?: "no nickname "
        println("$firstName ($nickNameToPrint) $lastName")
    }

}
