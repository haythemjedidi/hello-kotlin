import java.util.*
import kotlin.math.E

enum class EntityType{
    HELP, EASY, MEDIUM, HARD;
    fun getFormattedName() = name.toLowerCase().capitalize()

    fun getSpecialformatName(): String{
        val s = when(name){
            "EASY" -> "Easy measy"
            "MEDIUM" -> "hakkak"
            "HARD" -> "7jar"
            "HELP" -> "HELP"
            else -> "I don't know Baby"
        }
        return s;
    }
}


object EntityFactory{
    fun create(type: EntityType) : Entity {
        val id = UUID.randomUUID().toString()
        val name = when(type){
            EntityType.EASY -> type.getSpecialformatName()
            EntityType.MEDIUM -> type.getSpecialformatName()
            EntityType.HARD -> type.getSpecialformatName()
            EntityType.HELP -> type.getSpecialformatName()
        }
        return when(type){
            EntityType.EASY -> Entity.Easy(id, name)
            EntityType.MEDIUM -> Entity.Medium(id, name)
            EntityType.HARD -> Entity.Hard(id, name, 2f)
            EntityType.HELP -> Entity.Help
        }
    }
}

sealed class Entity (){
    object Help: Entity(){
        val name: String = "Help"
        override fun toString(): String {
            return "Help: $name"
        }
    }
    data class Easy(val id: String, val name: String): Entity(){
        override fun toString(): String {
            return "Easy: $id : $name"
        }
    }
    data class Medium(val id: String, val name: String): Entity(){
        override fun toString(): String {
            return "Medium: $id : $name"
        }
    }
    data class Hard(val id: String, val name: String, val multiplier: Float): Entity(){
        override fun toString(): String {
            return "Hard: $id : $name : $multiplier"
        }
    }


}
class TestTest(val firstPar: String = "1", val secondPar: String = "2"){
}


fun Entity.Medium.printInfo(){
    println("Medieum Class! $id")
}

val Entity.Medium.info: String
    get() = "Some Info"

fun main(){
    val easyEntity = EntityFactory.create(EntityType.EASY)
    val mediumEntity = EntityFactory.create(EntityType.MEDIUM)
    val hardEntity = EntityFactory.create(EntityType.HARD)
    val helpEntity = EntityFactory.create(EntityType.HELP)

    println("------------------------------------------------------------------\n" +
            "------------------------------------------------------------------\n" +
            "-------------------------------------------------------------------")
    val msg =when(easyEntity){
        Entity.Help -> "Help Class"
        is Entity.Easy -> "Easy Class"
        is Entity.Medium -> "Medium Class"
        is Entity.Hard -> "Hard Class"
    }
    println(msg)
    println("------------------------------------------------------------------\n" +
            "------------------------------------------------------------------\n" +
            "-------------------------------------------------------------------")

    val entity1 = Entity.Easy("id", "name")
    val entity3 = Entity.Easy("id", "name")
    val entity2 = entity1.copy(name = "new Name")
    if (entity1 === entity3 ){
        println("equal")
    }else {
        println("Not Equal")
    }

    val t1: TestTest = TestTest();
    val t2: TestTest = TestTest();
    if (t1 == t2 ){
        println("t1 and t2 are equal")
    }else {
        println("t1 and t2 are not Equal")
    }

    val secondMediumEntity = Entity.Medium("custom Id", "name")
    secondMediumEntity.printInfo()
    println(secondMediumEntity.info)

}