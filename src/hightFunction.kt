fun printFiltredString(list: List<String>, predicate: ((String) -> Boolean)?){
    list.forEach {
        if(predicate?.invoke(it) == true){
            println(it)
        }
    }
}

val predicate: (String) -> Boolean = {
    it.startsWith("G") or it.startsWith("g")
}
fun getPrintPredicate(): (String) -> Boolean{
    return {it.startsWith("g")}
}

fun main() {
    val list = listOf("kotlin", "Java", "js")


    printFiltredString(list) {it.startsWith("j")}

    val secondList = listOf("Docker", "gitlab", "gnu", "gnome", "gimp")
    printFiltredString(secondList, predicate)
    println("------------------------------------------------------")
    println("------------------------------------------------------")
    println("------------------------------------------------------")
    println("------------------------------------------------------")
    printFiltredString(secondList, getPrintPredicate())

}